from settings.configs.base import *

DEBUG = False


DOMAIN = env("DOMAIN")
DOMAINS = env("DJANGO_ALLOWED_HOSTS").split(' ')
SESSION_COOKIE_DOMAIN = DOMAIN
CSRF_COOKIE_DOMAIN = DOMAIN
ALLOWED_HOSTS = DOMAINS
SITE_ID = 1

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env("SQL_DATABASE"),
        'USER': env("SQL_USER"),
        'PASSWORD': env("SQL_PASSWORD"),
        'HOST': env("SQL_HOST"),
        'DISABLE_SERVER_SIDE_CURSORS': True,
    }
})
