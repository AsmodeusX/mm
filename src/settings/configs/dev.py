from settings.configs.base import *

DEBUG = True


DOMAIN = '127.0.0.1'

EMAIL_PORT = 587
EMAIL_USE_SSL = False
EMAIL_USE_TLS = True
SECURE_SSL_REDIRECT = False
ALLOWED_HOSTS = (
    DOMAIN,
    'localhost'
)
DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env("SQL_DATABASE"),
        'USER': env("SQL_USER"),
        'PASSWORD': env("SQL_PASSWORD"),
        'HOST': env("SQL_HOST"),
        'DISABLE_SERVER_SIDE_CURSORS': True,
    }
})
