from django.contrib.sitemaps import GenericSitemap
from main.models import MainPageConfig


main_page = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}


sitemaps = {
    'main': GenericSitemap(main_page, changefreq='daily', priority=1),
}
