from django.http import JsonResponse
from django.core import serializers


def json_serialize(data):
  return serializers.serialize("json", data)


def json_success(data):
  return JsonResponse(data, status=200)


def json_bad(msg=object):
  return JsonResponse(msg, status=404)
