from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from default_models.models import SEOPageConfig
from galleryfield.fields import GalleryField
from main.models import MainPageConfig


class Product(models.Model):
    name = models.CharField(_('name'), max_length=64, )
    preview = models.ImageField(_('preview'))
    gallery = GalleryField("galleryfield.BuiltInGalleryImage", verbose_name=_('gallery'), blank=True, null=True)
    note = models.TextField(_('note'), blank=True, )
    price = models.DecimalField(_('price'), decimal_places=2, max_digits=10)

    class Meta:
        default_permissions = ('change', )
        verbose_name = _('product')
        verbose_name_plural = _('products')

    def __str__(self):
        return self.name

    def list_gallery(self):
        arr = []
        for x in self.gallery.objects.all():
            arr.append(x.image.__str__())
        return arr

    @staticmethod
    def buy_link():
        main_config = MainPageConfig.get_solo()
        return main_config.tg_link
