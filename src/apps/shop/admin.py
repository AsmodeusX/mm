from django.contrib import admin
from django import forms
from solo.admin import SingletonModelAdmin
from galleryfield.mixins import GalleryFormMediaMixin
from .models import Product


class ProductAdminForm(GalleryFormMediaMixin, forms.ModelForm):
    class Meta:
        model = Product
        exclude = ()


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'name', 'note', 'price', 'preview', 'gallery',
            ),
        }),
    )
    form = ProductAdminForm

