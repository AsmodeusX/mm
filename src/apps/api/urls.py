from django.urls import path
from . import api

app_name = 'api'
urlpatterns = [
  path('', api.MainView.as_view(), name='main'),
  path('shop/', api.ProductsView.as_view(), name='products'),
  path('shop/<product_id>/', api.ProductView.as_view(), name='product'),
]
