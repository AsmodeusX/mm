from rest_framework import serializers
from shop.models import Product
from main.models import MainPageConfig


class MainSerializer(serializers.ModelSerializer):

    class Meta:
        model = MainPageConfig
        fields = ('header', 'description', 'title', 'tg_link', 'seo')

    @staticmethod
    def seo(obj):
        return obj.seo


class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("id", 'name', 'preview')


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("id", 'name', 'note', 'price', 'list_gallery', 'buy_link')