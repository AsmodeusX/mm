from django.views.generic import View
from django.utils.translation import ugettext_lazy as _

from libs.json_return import json_success

from .views.main import query_main
from .views.shop import query_products, query_product


from .serializers import (
    MainSerializer, ProductsSerializer, ProductSerializer
)


ERROR_MESSAGES = {
    'unknown_slug': _('Slug is unknown')
}


def format_serialize_data(serializer):
    return {
        'response': serializer.data,
    }


class MainView(View):
    def get(self, request, *args, **kwargs):
        data = query_main()

        serializer = MainSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)

        return json_success(serialized_data)


class ProductsView(View):
    def get(self, request, *args, **kwargs):
        data = query_products()

        serializer = ProductsSerializer(data, context={
            'request': request,
        }, many=True)

        serialized_data = format_serialize_data(serializer)
        return json_success(serialized_data)


class ProductView(View):
    def get(self, request, product_id, *args, **kwargs):
        data = query_product(product_id)

        serializer = ProductSerializer(data, context={
            'request': request,
        }, many=False)

        serialized_data = format_serialize_data(serializer)
        return json_success(serialized_data)
