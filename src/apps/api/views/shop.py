from shop.models import Product


def query_products():
    products = Product.objects.all()

    return products


def query_product(product_id):
    product = Product.objects.get(id=product_id)

    return product
