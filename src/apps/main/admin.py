from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import MainPageConfig
from default_models.admin import SEOPageConfigAdmin


@admin.register(MainPageConfig)
class MainPageConfigAdmin(SingletonModelAdmin, SEOPageConfigAdmin):
    fieldsets = SEOPageConfigAdmin.fieldsets + (
        (None, {
            'fields': (
                'tg_link',
            ),
        }),
    )

