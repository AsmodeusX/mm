from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from solo.admin import SingletonModelAdmin


class PageConfigAdmin(SingletonModelAdmin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'title', 'header', 'description',
            ),
        }),
    )


class SEOPageConfigAdmin(admin.ModelAdmin):
    fieldsets = PageConfigAdmin.fieldsets + (
        (_('SEO'), {
            'fields': (
                'seo_title', 'seo_description', 'seo_keywords',
            ),
        }),
        (_('OpenGraph'), {
            'fields': (
                'seo_og_title', 'seo_og_description', 'seo_og_site_name', 'seo_og_image',
            ),
        }),
    )
