import Head from 'next/head'
import styles from '../styles/Home.module.scss'
import Info from "../components/info/info"
import Gallery from "../components/gallery/gallery";
import useSWR from 'swr'

const fetcher = (...args) => fetch(...args).then((res) => res.json())

export default function Home() {
    const { data, error } = useSWR(`${process.env.API_HOST}/`, fetcher);
    if (error) return <div></div>
    if (!data) return <div></div>
    return (
        <>
            <Head>
                <title>{data.response.title}</title>
                <meta name="description" content={data.response.seo.seo_title} />
                <meta name="description" content={data.response.seo.seo_og_title} />
                <meta name="og_description" content={data.response.seo.seo_og_description} />
                <meta name="og_url" content={data.response.seo.seo_og_url} />
                <meta name="og_image" content={data.response.seo.seo_og_image} />
                <meta name="og_site_name" content={data.response.seo.seo_og_site_name} />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/logo.jpg" type={"image/jpeg"} />
                {/*<link rel="preconnect" href="https://fonts.googleapis.com" />*/}
                {/*<link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />*/}
                {/*<link href="https://fonts.googleapis.com/css2?family=Anton&family=Lato&display=swap" rel="stylesheet" />*/}
            </Head>
            <main className={styles.main}>
                <Info />
                <Gallery />
            </main>
        </>
    )
}
