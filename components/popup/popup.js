import Product from "../product/product";

function disablePopup(props) {
    props.setActive(null);
    document.getElementsByTagName("html")[0].classList.remove("no-scroll")
}

function checkClick(e, props) {
    if (e.target.classList.contains("popup-wrapper")) {
        disablePopup(props);
    }
}

function Popup(props) {
        if (!(props.active === null)) {
            document.getElementsByTagName("html")[0].classList.add("no-scroll")
            return (
                <div className={"popup-wrapper"} onClick={(e) => checkClick(e, props)}>
                    <div className={"popup"}>
                        <div className="popup__close" onClick={() => disablePopup(props)}>x</div>
                        <div className="popup__content"><Product id={props.active}/></div>
                    </div>
                </div>
            )
        }
}

export default Popup