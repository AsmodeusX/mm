import useSWR from 'swr'

const fetcher = (...args) => fetch(...args).then((res) => res.json())


function Info() {
    const { data, error } = useSWR(`${process.env.API_HOST}/`, fetcher);
    if (error) return <div>Failed to load</div>
    if (!data) return <div>Loading...</div>

    return (
        <div className={"info"}>
            <div className="header">{data.response.header}</div>
            <span className={"description"}>
                <p dangerouslySetInnerHTML={{ __html: data.response.description }}></p>
            </span>

            <div className={"contacts"}>
                Ссылки для связи с нашим магазином:
                <div className={"contacts__link contacts__link--telegram"}>
                    <a target={"_blank"} rel={"nofollow noopener noreferrer"} href={data.response.tg_link}>
                        &#9755;Telegram
                    </a>
                </div>
            </div>
        </div>
    )
}

export default Info