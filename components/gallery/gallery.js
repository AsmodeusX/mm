import useSWR from 'swr'
import ReactDOM from 'react-dom/client'
import Product from "../product/product";
import Popup from "../popup/popup";
import {useState} from "react";



const fetcher = (...args) => fetch(...args).then((res) => res.json())


function Gallery() {
    const [popupActive, setPopupActive] = useState(null)
    const { data, error } = useSWR(`${process.env.API_HOST}/shop/`, fetcher);
    if (error) return <div>Failed to load</div>
    if (!data) return <div>Loading...</div>
    return (
        <div className={"gallery"}>
            {data.response.map(({id, name, preview}) => {
                // return <div>{note} </div>
                return <div className={"gallery__preview-wrapper"} key={id}>
                    <div className={"gallery__preview"} onClick={(event) => {
                        event.stopPropagation();
                        setPopupActive(id)
                    }}>
                        <img src={preview} alt={name}/>
                    </div>
                </div>
            })}
        <Popup active={popupActive} setActive={setPopupActive}/>
        </div>
    )
    // } else return ""
}

export default Gallery