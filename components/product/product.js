import useSWR from "swr";
import { Swiper, SwiperSlide } from 'swiper/react';
import { EffectCards } from "swiper";
import 'swiper/css';
import "swiper/css/effect-cards";


const fetcher = (...args) => fetch(...args).then((res) => res.json())

function setSlide(href, alt) {
    const h = `${process.env.MEDIA_HOST}/${href}`;
    return <SwiperSlide><img className={"product__image"} src={h} alt={alt}/></SwiperSlide>
}

function Product(product) {
    const { data, error } = useSWR(`${process.env.API_HOST}/shop/${product.id}/`, fetcher);
    if (error) return <div>Failed to load</div>
    if (!data) return <div>Loading...</div>

    const product_data = data.response;
    return <div className={"product"}>
        <div className="product__part">

            <Swiper
                spaceBetween={0}
                slidesPerView={1}
                effect={"cards"}
        grabCursor={true}
        modules={[EffectCards]}>
                {product_data.list_gallery.map((href) =>  setSlide(href, product_data.name))}
            </Swiper>
        </div>
        <div className="product__part product__part--info">

            <div className="product__name">{product_data.name}</div>
            <div className="product__note">{product_data.note}</div>
            <div className="product__price">Цена: &#8381;{product_data.price}</div>
            <div className={"contacts__link contacts__link--telegram"}>
                    <a className={"product__link"} target={"_blank"} rel={"nofollow noopener noreferrer"} href={product_data.buy_link}>
                        &#9755;Хочу купить
                    </a>
                </div>
        </div>
    </div>
    // });
}
export default Product;